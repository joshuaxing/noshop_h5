export function setItem (key, value) {
  window.localStorage.setItem(key, JSON.stringify(value));
}
export function getItem (key) {
  let value = window.localStorage.getItem(key);
  let data = JSON.parse(value);
  return data;
}