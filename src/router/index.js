import Vue from 'vue'
import Router from 'vue-router'
import store from '../vuex'
import {getQueryString} from '../assets/js/handleUrl'
Vue.use(Router) 
const GoodsDetail = () => import('@/components/goods')
const ActivityDetail = () => import('@/components/activity')
const NOPage = () => import('@/components/404')
const NOConetnt = () => import('@/components/nocontent')
let router = new Router({
  routes: [
    {
      name: 'goodsDetail',
      path: '/goodsDetail',
      component: GoodsDetail,
      meta: { requiresAuth: true }
    },
    {
      name: 'activityDetail',
      path: '/activityDetail',
      component: ActivityDetail,
      meta: { requiresAuth: true }
    },
    {
      name: 'no',
      path: '/no',
      component: NOConetnt,
      meta: { requiresAuth: true }
    },
    {
      name: '404',
      path: '/404',
      component: NOPage
    }
  ]
})
router.beforeEach((to, from, next) => {
  let id = getQueryString('id');
  let userId = getQueryString('userId');
  console.log(id)
  console.log(userId)
  if (id) {
    store.state.id = parseInt(id);
  } 
  if (userId) {
    store.state.userId = parseInt(userId);
  } else {
    store.state.userId = 0;
  }
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!parseInt(id)) {
      next({
        path: '/404'
      })
    } else {
      next()
    }
  } else {
    next()
     // 确保一定要调用 next()
  }
})

export default router;