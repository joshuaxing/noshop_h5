import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    id: 0,
    userId: 0
  },
  getters: {

  },
  mutations: {
    
  },
  actions: {

  }
})

export default store
